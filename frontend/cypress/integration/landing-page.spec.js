describe('When loading Landing Page', () => {
  it('Should load', () => {
    cy.visit('http://localhost:3000');
  });

  it('Should go to Booking page when clicking CTA', () => {
    cy.visit('http://localhost:3000');
    cy.contains('BOOK A FLIGHT!').click();
    cy.url().should('eql', 'http://localhost:3000/booking');
  });
});
