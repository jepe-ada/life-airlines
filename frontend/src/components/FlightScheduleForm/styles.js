import styled from 'styled-components';

export const Container = styled.div`
  width: 50vw;
  height: 40vh;
  background-image: linear-gradient(to right, #f80759, #bc4e9c);
  transform: translateX(-5vw);
  border-radius: 5px;
  box-shadow: 0 40px 100px -40px #f80759;
  padding: 20px;
`;

export const TextInput = styled.input.attrs({ type: 'text' })`
  background: transparent;
  border: none;
  outline: none;
  padding-bottom: 8px;
  margin: 5px;
  padding-left: 2px;
  font-size: 1.5rem;
  border-bottom: 1px solid #eeeeee;
  color: #fff;
  font-weight: 700;
`;
