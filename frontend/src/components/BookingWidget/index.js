import React from 'react';
import FlightTypeContainer from '../../containers/FlightTypeContainer';
import FlightScheduleForm from '../FlightScheduleForm';

const BookingWidget = () => {
  return (
    <FlightTypeContainer>
      <FlightScheduleForm />
    </FlightTypeContainer>
  );
};

export default BookingWidget;
