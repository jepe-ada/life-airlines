import styled from 'styled-components';

export const Container = styled.div`
  background-color: #333;
  width: 40vw;
  height: 50vh;
  box-shadow: 0 0 40px -10px #333;
  border-radius: 5px;
`;

export const SelectionContainer = styled.div`
  display: flex;
`;

export const Button = styled.div`
  flex: 1;
  text-align: center;
  color: ${props =>
    'selected' in props && props.selected ? '#bdbdbd' : '#757575'};
  padding: 25px;
  cursor: pointer;
  border-right: 1px solid #424242;
  transition: all 0.25s;
  border-radius: 5px;
  background-color: ${props =>
    'selected' in props && props.selected ? '#424242' : '#333'};

  &:hover {
    background-color: #424242;
    color: #bdbdbd;
  }
`;
