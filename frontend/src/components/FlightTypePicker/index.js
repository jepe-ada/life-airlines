import React from 'react';
import { Button, Container, SelectionContainer } from './styles';

const FlightTypePicker = ({ children, flightType, setFlightType }) => {
  const setTypeTo = type => () => setFlightType(type);
  const setToReturn = setTypeTo('RETURN');
  const setToOneWay = setTypeTo('ONE_WAY');
  const setToMultiCity = setTypeTo('MULTI_CITY');

  const isReturn = flightType === 'RETURN';
  const isOneWay = flightType === 'ONE_WAY';
  const isMultiCity = flightType === 'MULTI_CITY';

  return (
    <Container>
      <SelectionContainer>
        <Button selected={isReturn} onClick={setToReturn}>
          RETURN
        </Button>
        <Button selected={isOneWay} onClick={setToOneWay}>
          ONE WAY
        </Button>
        <Button selected={isMultiCity} onClick={setToMultiCity}>
          MULTI-CITY
        </Button>
      </SelectionContainer>
      {children}
    </Container>
  );
};

export default FlightTypePicker;
