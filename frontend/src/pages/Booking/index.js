import React from 'react';
import BookingWidget from '../../components/BookingWidget';
import { Container } from './styles';

const Booking = () => {
  return (
    <Container>
      <BookingWidget />
    </Container>
  );
};

export default Booking;
