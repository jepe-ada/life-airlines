import styled from 'styled-components';
import FullContainer from '../../components/FullContainer';

export const Container = styled(FullContainer)`
  background-image: linear-gradient(to bottom right, #fbc7d4, #9796f0);
  display: flex;
  justify-content: center;
  align-items: center;
`;
