import React from 'react';
import {
  BigHeader,
  CTA,
  Hero,
  HeroContent,
  HeroText,
  SubHeader
} from './styles';

const Landing = () => {
  return (
    <Hero>
      <HeroContent>
        <BigHeader>Hello,</BigHeader>
        <SubHeader>We are Life Airlines.</SubHeader>
        <HeroText>We'll help you get the best bang for your buck.</HeroText>
        <CTA to="/booking">BOOK A FLIGHT!</CTA>
      </HeroContent>
    </Hero>
  );
};

export default Landing;
