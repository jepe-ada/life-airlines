import { Link } from '@reach/router';
import styled from 'styled-components';
import landingBg from '../../assets/images/landing-bg.jpg';
import FullContainer from '../../components/FullContainer';

export const Hero = styled(FullContainer)`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-template-rows: 3rem auto;
  background-image: url(${landingBg});
  bacgkround-size: cover;
  background-position: center;
`;

export const HeroContent = styled.div`
  grid-row: 2 / span 1;
  grid-column: 1 / span 1;
  display: flex;
  flex-direction: column;
  padding: 1rem;
  padding-left: 6rem;
`;

export const BigHeader = styled.h2`
  color: #000063;
  font-weight: 900;
  font-size: 6rem;
  margin-top: 25vh;
  margin-bottom: 0;
`;

export const SubHeader = styled.h3`
  color: #000063;
  font-weight: 700;
  font-size: 2rem;
  margin: 0;
`;

export const HeroText = styled.h3`
  color: #000063;
  font-weight: 200;
  font-size: 1.5rem;
`;

export const CTA = styled(Link)`
  border: none;
  padding: 20px 15px;
  color: white;
  background-color: ${props => ('color' in props ? props.color : '#f44336')};
  border-radius: 5px;
  box-shadow: 0 15px 40px -15px ${props => ('color' in props ? props.color : '#f44336')};
  width: 150px;
  font-weight: 400;
  font-size: 0.8em;
  cursor: pointer;
  outline: none;
  transition: all 0.2s;
  text-decoration: none;
  text-align: center;

  &:hover {
    background-color: #f6685e;
  }
`;
