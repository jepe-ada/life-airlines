import styled from 'styled-components';
import bg from '../../assets/images/signup-bg.jpg';
import FullContainer from '../../components/FullContainer';

export const Container = styled(FullContainer)`
  background-size: cover;
  background-position: center;
  background-image: url(${bg});
`;
