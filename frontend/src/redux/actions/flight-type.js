export const SET_FLIGHT_TYPE = 'SET_FLIGHT_TYPE';

export const setFlightType = flightType => ({
  type: SET_FLIGHT_TYPE,
  payload: flightType
});
