import { all, put } from 'redux-saga/effects';

function* helloSaga() {
  yield put({
    type: 'LOGIN',
    payload: { username: 'user', password: 'password' }
  });
}

export default function* rootSaga() {
  yield all([helloSaga()]);
}
