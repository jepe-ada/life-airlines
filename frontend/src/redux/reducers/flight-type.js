import { SET_FLIGHT_TYPE } from '../actions/flight-type';

const defaultState = 'RETURN';

const flightType = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FLIGHT_TYPE:
      return action.payload;
    default:
      return state;
  }
};

export default flightType;
