import { LOGIN } from '../actions/auth';

const auth = (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default auth;
