import { combineReducers } from 'redux';
import auth from './auth';
import flightType from './flight-type';

export default combineReducers({
  auth,
  flightType
});
