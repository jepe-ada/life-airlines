import React from 'react';
import { connect } from 'react-redux';
import FlightTypePicker from '../components/FlightTypePicker';
import { setFlightType } from '../redux/actions/flight-type';

const FlightTypeContainer = ({ children, flightType, setFlightType }) => {
  return (
    <FlightTypePicker flightType={flightType} setFlightType={setFlightType}>
      {children}
    </FlightTypePicker>
  );
};

const mapStateToProps = state => ({
  flightType: state.flightType
});

const mapDispatchToProps = dispatch => ({
  setFlightType: type => dispatch(setFlightType(type))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(FlightTypeContainer));
