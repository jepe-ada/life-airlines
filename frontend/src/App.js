// @flow

import React from 'react';
import { Provider } from 'react-redux';
import AnimationRouter from './AnimationRouter';
import Booking from './pages/Booking';
import Landing from './pages/Landing';
import Signup from './pages/Signup';
import store from './redux';

const App = () => {
  return (
    <Provider store={store}>
      <AnimationRouter>
        <Landing path="/" />
        <Signup path="/signup" />
        <Booking path="/booking" />
      </AnimationRouter>
    </Provider>
  );
};

export default App;
