# Life Airlines

A sample flight booking application for training.

## Quickstart

1. Install Docker (or Docker Toolbox on Windows **EW**).
2. Install Node.
3. Install Yarn.
4. Go to the `frontend` directory with `cd frontend`.
5. Run `yarn` to install dependencies.
6. Go to root directory with `cd ..`.
7. Run `docker-compose up`.
8. Open app at `http://localhost:3000`.

## Stack

### Frontend

1. React
2. Redux
3. [Redux Saga](https://redux-saga.js.org/) - _Redux middleware for asynchronous
   actions without callback hell and action impurity._
4. [react-testing-library](https://www.npmjs.com/package/react-testing-library) -
   _Simple and complete React DOM testing utilities that encourage good testing
   practices._
5. [Apollo](https://www.apollographql.com/docs/react/) - _GraphQL Client for
   React._
6. [fbt](https://facebookincubator.github.io/fbt/) - _Facebook's
   Internationalization Framework._
7. [Reach Router](https://reach.tech/router) - _A React router with better
   accessibility features._
8. [Helmet](https://github.com/nfl/react-helmet) - _Adding content to the head
   of a page._
9. [react-snap](https://www.npmjs.com/package/react-snap) - _React app
   pre-rendering for SEO._
10. [Storybook](https://storybook.js.org/) - _A tool for building UI components
    for the web._
11. [Cypress](https://www.cypress.io/) - _A better alternative to Selenium for
    End-to-End testing._
12. [Flow](https://flow.org/en/) - _A static type checker for JavaScript._
13. [jsverify](http://jsverify.github.io/) - _A library for Property-Based
    Testing._
14. [Styled Components](https://www.styled-components.com/) - _CSS-in-JS
    implementation in React._
15. [Popmotion Pose](https://popmotion.io/pose/) - _A declarative animation
    library._

### Backend

1. Java

### Mobile

1. Flutter
